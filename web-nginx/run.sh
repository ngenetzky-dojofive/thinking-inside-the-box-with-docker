#!/bin/sh
docker run --detach --rm \
    --name "web-nginx-container" \
    --publish 80:80 \
    'web-nginx-image'
