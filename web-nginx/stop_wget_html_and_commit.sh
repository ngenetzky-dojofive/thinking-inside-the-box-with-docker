#!/bin/sh
docker rm -f 'web-nginx-container' >/dev/null 2>&1 || true

docker run -t \
    --name 'web-nginx-container' \
    'web-nginx-image' \
    wget \
    'https://gist.github.com/physacco/2e1b52415f3a964ad2a542a99bebed8f' \
    -O '/var/www/wget-gist-from-github.html'

docker commit \
    'web-nginx-container' \
    'web-nginx-with-data-image:1.0'

# Clean up container after its commited
docker rm -f 'web-nginx-container' >/dev/null 2>&1 || true
