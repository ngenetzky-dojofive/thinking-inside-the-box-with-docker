#!/bin/bash

DEFAULT_A='web-nginx-image'
DEFAULT_B='web-nginx-with-data-image:1.0'

inspect_and_compare(){
    local a b format
    a="${1-${DEFAULT_A}}"
    b="${2-${DEFAULT_B}}"
    format='{{ json .Config.Cmd }}'

    diff \
        <(docker inspect "${a}" --format "${format}") \
        <(docker inspect "${b}" --format "${format}")
}

inspect_and_compare "$@"
