#!/bin/sh
SCRIPTDIR="$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)"
cd "${SCRIPTDIR}"

docker rm -f 'web-nginx-container' >/dev/null 2>&1 || true

docker run --detach --rm \
    --name "web-nginx-container" \
    --volume "$(pwd):/var/www/mounted-pwd" \
    --publish 80:80 \
    'web-nginx-with-data-image:1.0' \
    'nginx'

xdg-open 'http://localhost:80'
