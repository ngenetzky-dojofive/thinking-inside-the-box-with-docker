#!/bin/sh
docker run --rm \
    --name "cli-pyflame-container" \
    'cli-pyflame-image' "$@"
