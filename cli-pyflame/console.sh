#!/bin/sh
docker run -it --rm \
    --entrypoint '/bin/bash' \
    --name "cli-pyflame-container" \
    'cli-pyflame-image' \
    "$@"
