#!/bin/sh
docker run -it --rm \
    --entrypoint 'python -m pyflame.flame lite' \
    --name "cli-pyflame-container" \
    'cli-pyflame-image' \
    'lite'
    "$@"
