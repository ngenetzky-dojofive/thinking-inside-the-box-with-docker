#!/bin/sh
SCRIPTDIR="$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)"
cd "${SCRIPTDIR}"
docker build \
    --tag 'cli-pyflame-image' \
    './'
