#!/usr/bin/env python

import logging

import fire

class Flame(object):
    def lite(self, n: int, of: str = 'candles'):
        logging.warning('lit {0} {1} on fire'.format(n, of))

def main():
    fire.Fire(Flame())

if __name__ == "__main__":
    main()
