#!/usr/bin/env python
from setuptools import setup

setup(
    name='pyflame',
    keywords='Trivial python cli built on Fire',
    packages=['pyflame'],
    install_requires=['fire'],
)
